const express = require('express');
const { createProxyMiddleware } = require('http-proxy-middleware');
const https = require('https');
const path = require('path');
const fs = require('fs');
require('dotenv').config();

const app = express();
const portNumber = process.env.PORT || 3080;

const sslServer = https.createServer({
    key: fs.readFileSync(path.join(__dirname, 'cert', 'key.pem')),
    cert: fs.readFileSync(path.join(__dirname, 'cert', 'cert.pem'))
}, app);

// Proxy middleware options
const s3uploaderProxy = {
    target: process.env.S3_UPLOADER_URL || 'http://52.202.51.20:3004',
    changeOrigin: true,
    secure: false, // Set to true if the target URL uses a self-signed certificate
    logLevel: 'debug' // Set to 'warn' or 'error' for production
};

const wooserverProxy = {
    target: process.env.WOOSERVER_URL || 'http://34.234.73.211:7078',
    changeOrigin: true,
    secure: false, // Set to true if the target URL uses a self-signed certificate
    logLevel: 'debug' // Set to 'warn' or 'error' for production
};

// Create the proxy middleware
app.use('/uploadS3', createProxyMiddleware(s3uploaderProxy));
app.use('/publishProduct', createProxyMiddleware(wooserverProxy));
app.use('/retrieveVariant', createProxyMiddleware(wooserverProxy));
app.use('/retrieve', createProxyMiddleware(wooserverProxy));
app.use('/order-update', createProxyMiddleware(wooserverProxy));
app.use('/update', createProxyMiddleware(wooserverProxy));
app.use('/update-variation', createProxyMiddleware(wooserverProxy));
app.use('/delete', createProxyMiddleware(wooserverProxy));
app.use('/product-variation', createProxyMiddleware(wooserverProxy));

// Start the server
sslServer.listen(portNumber, () => console.log(`Secure server running on port ${portNumber}`));
